#!/bin/bash

# Freebayes varient calling wrapper script


###############################################################################
#### Helper Functions ####
###############################################################################

## ****************************************************************************
## Usage description should match command line arguments defined below
usage () {
    echo "Usage: $(basename "$0")"
    echo "  --input => Directory with sorted and indexed BAM file"
    echo "  --reference => Reference FASTA file"
    echo "  --min_quality => Minimum quality score"
    echo "  --min_coverage => Minimum coverage"
    echo "  --min_probability => Minimum probability that a variant exists"
    echo "  --min_fraction => Minimum fraction of sample that shows the variant"
    echo "  --ploidy => Ploidy of the sample"
    echo "  --output => Output VCF File"
    echo "  --exec_method => Execution method (singularity, auto)"
    echo "  --exec_init => Execution initialization command(s)"
    echo "  --help => Display this help message"
}
## ****************************************************************************

# report error code for command
safeRunCommand() {
    cmd="$@"
    eval "$cmd; "'PIPESTAT=("${PIPESTATUS[@]}")'
    for i in ${!PIPESTAT[@]}; do
        if [ ${PIPESTAT[$i]} -ne 0 ]; then
            echo "Error when executing command #${i}: '${cmd}'"
            exit ${PIPESTAT[$i]}
        fi
    done
}

# print message and exit
fail() {
    msg="$@"
    echo "${msg}"
    usage
    exit 1
}

# always report exit code
reportExit() {
    rv=$?
    echo "Exit code: ${rv}"
    exit $rv
}

trap "reportExit" EXIT

# check if string contains another string
contains() {
    string="$1"
    substring="$2"

    if test "${string#*$substring}" != "$string"; then
        return 0    # $substring is not in $string
    else
        return 1    # $substring is in $string
    fi
}



###############################################################################
## SCRIPT_DIR: directory of current script, depends on execution
## environment, which may be detectable using environment variables
###############################################################################
if [ -z "${AGAVE_JOB_ID}" ]; then
    # not an agave job
    SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
else
    echo "Agave job detected"
    SCRIPT_DIR=$(pwd)
fi
## ****************************************************************************



###############################################################################
#### Parse Command-Line Arguments ####
###############################################################################

getopt --test > /dev/null
if [ $? -ne 4 ]; then
    echo "`getopt --test` failed in this environment."
    exit 1
fi

## ****************************************************************************
## Command line options should match usage description
OPTIONS=
LONGOPTIONS=help,exec_method:,exec_init:,input:,reference:,min_quality:,min_coverage:,min_probability:,min_fraction:,ploidy:,output:,
## ****************************************************************************

# -temporarily store output to be able to check for errors
# -e.g. use "--options" parameter by name to activate quoting/enhanced mode
# -pass arguments only via   -- "$@"   to separate them correctly
PARSED=$(\
    getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@"\
)
if [ $? -ne 0 ]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    usage
    exit 2
fi

# read getopt's output this way to handle the quoting right:
eval set -- "$PARSED"

## ****************************************************************************
## Set any defaults for command line options
MIN_FRACTION="0.05"
PLOIDY="1"
EXEC_METHOD="auto"
EXEC_INIT=":"
## ****************************************************************************

## ****************************************************************************
## Handle each command line option. Lower-case variables, e.g., ${file}, only
## exist if they are set as environment variables before script execution.
## Environment variables are used by Agave. If the environment variable is not
## set, the Upper-case variable, e.g., ${FILE}, is assigned from the command
## line parameter.
while true; do
    case "$1" in
        --help)
            usage
            exit 0
            ;;
        --input)
            if [ -z "${input}" ]; then
                INPUT=$2
            else
                INPUT="${input}"
            fi
            shift 2
            ;;
        --reference)
            if [ -z "${reference}" ]; then
                REFERENCE=$2
            else
                REFERENCE="${reference}"
            fi
            shift 2
            ;;
        --min_quality)
            if [ -z "${min_quality}" ]; then
                MIN_QUALITY=$2
            else
                MIN_QUALITY="${min_quality}"
            fi
            shift 2
            ;;
        --min_coverage)
            if [ -z "${min_coverage}" ]; then
                MIN_COVERAGE=$2
            else
                MIN_COVERAGE="${min_coverage}"
            fi
            shift 2
            ;;
        --min_probability)
            if [ -z "${min_probability}" ]; then
                MIN_PROBABILITY=$2
            else
                MIN_PROBABILITY="${min_probability}"
            fi
            shift 2
            ;;
        --min_fraction)
            if [ -z "${min_fraction}" ]; then
                MIN_FRACTION=$2
            else
                MIN_FRACTION="${min_fraction}"
            fi
            shift 2
            ;;
        --ploidy)
            if [ -z "${ploidy}" ]; then
                PLOIDY=$2
            else
                PLOIDY="${ploidy}"
            fi
            shift 2
            ;;
        --output)
            if [ -z "${output}" ]; then
                OUTPUT=$2
            else
                OUTPUT="${output}"
            fi
            shift 2
            ;;
        --exec_method)
            if [ -z "${exec_method}" ]; then
                EXEC_METHOD=$2
            else
                EXEC_METHOD="${exec_method}"
            fi
            shift 2
            ;;
        --exec_init)
            if [ -z "${exec_init}" ]; then
                EXEC_INIT=$2
            else
                EXEC_INIT="${exec_init}"
            fi
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Invalid option"
            usage
            exit 3
            ;;
    esac
done
## ****************************************************************************

## ****************************************************************************
## Log any variables passed as inputs
echo "Input: ${INPUT}"
echo "Reference: ${REFERENCE}"
echo "Min_quality: ${MIN_QUALITY}"
echo "Min_coverage: ${MIN_COVERAGE}"
echo "Min_probability: ${MIN_PROBABILITY}"
echo "Min_fraction: ${MIN_FRACTION}"
echo "Ploidy: ${PLOIDY}"
echo "Output: ${OUTPUT}"
echo "Execution Method: ${EXEC_METHOD}"
echo "Execution Initialization: ${EXEC_INIT}"
## ****************************************************************************



###############################################################################
#### Validate and Set Variables ####
###############################################################################

## ****************************************************************************
## Add app-specific logic for handling and parsing inputs and parameters

# INPUT input

if [ -z "${INPUT}" ]; then
    echo "Directory with sorted and indexed BAM file required"
    echo
    usage
    exit 1
fi
# make sure INPUT is staged
count=0
while [ ! -d "${INPUT}" ]
do
    echo "${INPUT} not staged, waiting..."
    sleep 1
    count=$((count+1))
    if [ $count == 10 ]; then break; fi
done
if [ ! -d "${INPUT}" ]; then
    echo "Directory with sorted and indexed BAM file not found: ${INPUT}"
    exit 1
fi
INPUT_FULL=$(readlink -f "${INPUT}")
INPUT_DIR=$(dirname "${INPUT_FULL}")
INPUT_BASE=$(basename "${INPUT_FULL}")


# REFERENCE input

if [ -z "${REFERENCE}" ]; then
    echo "Reference FASTA file required"
    echo
    usage
    exit 1
fi
# make sure REFERENCE is staged
count=0
while [ ! -f "${REFERENCE}" ]
do
    echo "${REFERENCE} not staged, waiting..."
    sleep 1
    count=$((count+1))
    if [ $count == 10 ]; then break; fi
done
if [ ! -f "${REFERENCE}" ]; then
    echo "Reference FASTA file not found: ${REFERENCE}"
    exit 1
fi
REFERENCE_FULL=$(readlink -f "${REFERENCE}")
REFERENCE_DIR=$(dirname "${REFERENCE_FULL}")
REFERENCE_BASE=$(basename "${REFERENCE_FULL}")



# MIN_QUALITY parameter
if [ -n "${MIN_QUALITY}" ]; then
    :
else
    :
fi


# MIN_COVERAGE parameter
if [ -n "${MIN_COVERAGE}" ]; then
    :
else
    :
fi


# MIN_PROBABILITY parameter
if [ -n "${MIN_PROBABILITY}" ]; then
    :
else
    :
fi


# MIN_FRACTION parameter
if [ -n "${MIN_FRACTION}" ]; then
    :
else
    :
fi


# PLOIDY parameter
if [ -n "${PLOIDY}" ]; then
    :
else
    :
fi


# OUTPUT parameter
if [ -n "${OUTPUT}" ]; then
    :
    OUTPUT_FULL=$(readlink -f "${OUTPUT}")
    OUTPUT_DIR=$(dirname "${OUTPUT_FULL}")
    OUTPUT_BASE=$(basename "${OUTPUT_FULL}")
    LOG_FULL="${OUTPUT_DIR}/_log"
    TMP_FULL="${OUTPUT_DIR}/_tmp"
else
    :
    echo "Output VCF File required"
    echo
    usage
    exit 1
fi

## ****************************************************************************

## EXEC_METHOD: execution method
## Suggested possible options:
##   auto: automatically determine execution method
##   singularity: singularity image packaged with the app
##   docker: docker containers from docker-hub
##   environment: binaries available in environment path

## ****************************************************************************
## List supported execution methods for this app (space delimited)
exec_methods="singularity auto"
## ****************************************************************************

## ****************************************************************************
# make sure the specified execution method is included in list
if ! contains " ${exec_methods} " " ${EXEC_METHOD} "; then
    echo "Invalid execution method: ${EXEC_METHOD}"
    echo
    usage
    exit 1
fi
## ****************************************************************************



###############################################################################
#### App Execution Initialization ####
###############################################################################

## ****************************************************************************
## Execute any "init" commands passed to the GeneFlow CLI
CMD="${EXEC_INIT}"
echo "CMD=${CMD}"
safeRunCommand "${CMD}"
## ****************************************************************************



###############################################################################
#### Auto-Detect Execution Method ####
###############################################################################

# assign to new variable in order to auto-detect after Agave
# substitution of EXEC_METHOD
AUTO_EXEC=${EXEC_METHOD}
## ****************************************************************************
## Add app-specific paths to detect the execution method.
if [ "${EXEC_METHOD}" = "auto" ]; then
    # detect execution method
    if command -v singularity >/dev/null 2>&1; then
        AUTO_EXEC=singularity
    else
        echo "Valid execution method not detected"
        echo
        usage
        exit 1
    fi
    echo "Detected Execution Method: ${AUTO_EXEC}"
fi
## ****************************************************************************



###############################################################################
#### App Execution Preparation, Common to all Exec Methods ####
###############################################################################

## ****************************************************************************
## Add logic to prepare environment for execution
MNT=""; ARG=""; CMD0="mkdir -p ${LOG_FULL} ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
## ****************************************************************************



###############################################################################
#### App Execution, Specific to each Exec Method ####
###############################################################################

## ****************************************************************************
## Add logic to execute app
## There should be one case statement for each item in $exec_methods
case "${AUTO_EXEC}" in
    singularity)
        MNT=""; ARG=""; MNT="${MNT} -B "; MNT="${MNT}\"${INPUT_DIR}:/data1\""; ARG="${ARG} \"/data1/${INPUT_BASE}/${INPUT_BASE}.bam\""; ARG="${ARG} -j"; ARG="${ARG} -f"; MNT="${MNT} -B "; MNT="${MNT}\"${REFERENCE_DIR}:/data3\""; ARG="${ARG} \"/data3/${REFERENCE_BASE}\""; ARG="${ARG} -q"; ARG="${ARG} \"${MIN_QUALITY}\""; ARG="${ARG} --min-coverage"; ARG="${ARG} \"${MIN_COVERAGE}\""; ARG="${ARG} -P"; ARG="${ARG} \"${MIN_PROBABILITY}\""; ARG="${ARG} -F"; ARG="${ARG} \"${MIN_FRACTION}\""; ARG="${ARG} -p"; ARG="${ARG} \"${PLOIDY}\""; ARG="${ARG} -v"; MNT="${MNT} -B "; MNT="${MNT}\"${OUTPUT_DIR}:/data9\""; ARG="${ARG} \"/data9/${OUTPUT_BASE}\""; CMD0="singularity -s exec ${MNT} docker://quay.io/biocontainers/freebayes:1.3.3--py37hc088bd4_0 freebayes ${ARG}"; CMD0="${CMD0} >\"${LOG_FULL}/${OUTPUT_BASE}-freebayes.stdout\""; CMD0="${CMD0} 2>\"${LOG_FULL}/${OUTPUT_BASE}-freebayes.stderr\""; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
        ;;
esac
## ****************************************************************************



###############################################################################
#### Cleanup, Common to All Exec Methods ####
###############################################################################

## ****************************************************************************
## Add logic to cleanup execution artifacts, if necessary
## ****************************************************************************

