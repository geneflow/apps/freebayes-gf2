Freebayes Geneflow2 App
=======================

Version: 1.3.3-01

This is a GeneFlow app uses Freebayes to detect variants and generates a vcf file.

Inputs
------

1. input: Directory that contains a sorted BAM file and index.
2. input: FASTA reference file

Parameters
----------
1. min_quality: Minimum quality score
2. min_coverage: require at least this coverage to process a site
3. min_probability: Minimum probability that a variant exists
4. min_fraction: Minimum fraction of sample that shows the variant
5. ploidy: Ploidy of the genome being examined
6. output: Output vcf file
